﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{
    bool end;
    public string shapeName;
    public List<Vector3> directions;
    public Quaternion upright;
    SpriteRenderer sr;
    public bool working;

    List<Vector3> directions2;

    LayerMask shapeMask = 512;

    Shape shapeCheck = null;

    private void Start()
    {
        upright = Quaternion.Euler(0, 0, 0);
        sr = GetComponent<SpriteRenderer>();
        directions2 = new List<Vector3>();
        directions2.Add(Vector3.up);
        directions2.Add(Vector3.right);
        directions2.Add(Vector3.up * -1);
        directions2.Add(Vector3.right * -1);
    }

    public void Rotate()
    {
        transform.rotation = upright;
    }

    public void SortLayer(int p)
    {
        GetComponent<SpriteRenderer>().sortingOrder = p;
    }

    public void MatchCheck()
    {

        if (!MatchThree())
        {
            sr.color = Color.white;
            working = false;
        }
        else
        {
            sr.color = new Color(0.4f,0.4f,0.4f,0.7f);
            working = true;
        }
    }


    bool MatchThree()
    {
        if (gameObject.layer != 9)
        {
            return false;
        }



        //for each four adjacent spaces,
        for (int d = 0; d < directions2.Count; d++)
        {



            Vector3 direction = /*transform.TransformDirection(*/directions2[d]/*)*/;
            Debug.DrawLine(transform.position + direction, transform.position + direction + transform.up * 0.1f, Color.red, 2f);

            //if there's a shape,
            if (ShapeCheck(direction) != null)
            {


                //check for match
                if (ShapeCheck(direction).shapeName == shapeName)
                {



                    //if opposite is a match

                    if (ShapeCheck(direction * -1) != null)
                    {
                        if (ShapeCheck(direction * -1).shapeName == shapeName)
                        {
                            return true;
                        }
                    }

                    //if the one next over is a match

                    //if opposite is a match

                    if (ShapeCheck(direction * 2) != null)
                    {
                        if (ShapeCheck(direction * 2).shapeName == shapeName)
                        {
                            return true;
                        }
                    }

                }

            }
        }



        //print("problems. lots and lots of problems.");
        return false;

    }


    Shape ShapeCheck(Vector3 k)
    {
        if (Physics2D.Raycast(transform.position + k, transform.up, 0.1f, shapeMask))
        {
            return Physics2D.Raycast(transform.position + k, transform.up, 0.1f, shapeMask).collider.gameObject.GetComponent<Shape>();
        }
        else
        {
            return null;
        }
    }
}