﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GridManager : MonoBehaviour
{
    float planeTimer;
    int planeTime = 20;
    float lastPlane;
    public AudioSource aS;
    public bool dam = false;

    public void Click()
    {
        aS.PlayOneShot(aS.clip);
    }

    private void Start()
    {
        aS = GetComponent<AudioSource>();
    }

    public bool somethingSelected = false;
    List<Vector3Int> tiles;
    public GameObject plane;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Jump") > 0)
        {
            Next();
        }

        if (Time.time - lastPlane > planeTimer)
        {

            lastPlane = Time.time;
            Instantiate(plane);
            planeTimer = Random.Range(20, 40);
        }   
    }

    public void Next()
    {
        if(Time.timeSinceLevelLoad>2)
             SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        
    }
    public void Back()
    {
        if (Time.timeSinceLevelLoad > 2)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
