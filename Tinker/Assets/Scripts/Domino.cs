﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Domino : MonoBehaviour
{
    bool selected = false;
    List<Shape> shapes;
    GridLayout gridLayout;
    public LayerMask shapeMask;
    Shape shapeCheck = null;
    [SerializeField] Vector2 driftSpeedRange;
    float driftSpeed;
    int rotationDir;
    public Quaternion upright;
    [SerializeField] Vector2 respawnRangeX = new Vector2(-8f, 8f);
    [SerializeField] Vector2 respawnRangeY = new Vector2(10f,20f);
    bool vertical = true;
    SpriteRenderer sr;
    Shape[] allShapes;
    StrengthChecker[] allStrengths;

    // Start is called before the first frame update
    void Start()
    {
        shapes = new List<Shape>();
        shapes.Add(transform.GetChild(0).GetComponent<Shape>());
        shapes.Add(transform.GetChild(1).GetComponent<Shape>());
        allShapes = FindObjectsOfType<Shape>();
        allStrengths = FindObjectsOfType<StrengthChecker>();
        Physics.queriesHitTriggers = true;
        gridLayout = GameObject.Find("Grid Layout").GetComponent<GridLayout>();
        driftSpeed = Random.Range(driftSpeedRange.x, driftSpeedRange.y);
        StartCoroutine("Drift");

        shapes[0].gameObject.layer = 10;
        shapes[1].gameObject.layer = 10;

        upright = Quaternion.Euler(0, 0, 0);

        if (Random.Range(0, 2) == 0)
        {
            rotationDir = 1;
        }
        else
        {
            rotationDir = -1;
        }
        sr = GetComponent<SpriteRenderer>();

        //sr.color = new Color(255, 255, 255, 0.6f);
    }

    void Respawn()
    {
        transform.position = new Vector3(Random.Range(respawnRangeX.x, respawnRangeX.y), Random.Range(respawnRangeY.x, respawnRangeY.y));
    }

    private void OnMouseDown()
    {

        


        if (!selected)
        {
            if (!gridLayout.GetComponent<GridManager>().somethingSelected)
            {
                StopAllCoroutines();
                Select();
            }
            
        }
        else
        {
            
            Deselect();
            //print("deselect");
        }

        if (gridLayout.GetComponent<GridManager>().dam)
        {
            for (int s = 0; s < allShapes.Length; s++)
            {
                allShapes[s].MatchCheck();

            }
            for (int s = 0; s < allStrengths.Length; s++)
            {
                allStrengths[s].ReportStrength();

            }
            FindObjectOfType<TotalScore>().Refresh();
        }
        



        //print("clicked");

    }//selection event controller

    void Select()
    {
        sr.color = Color.white;
        shapes[0].gameObject.layer = 10;
        shapes[1].gameObject.layer = 10;
        gridLayout.GetComponent<GridManager>().somethingSelected = true;
        StartCoroutine(Selected());
        selected = true;
        transform.rotation = upright;
        vertical = true;
        shapes[0].Rotate();
        shapes[1].Rotate();
        shapes[0].SortLayer(11);
        shapes[1].SortLayer(11);
        sr.sortingOrder = 10;
        
    }



    void Deselect()
    {
        gridLayout.GetComponent<GridManager>().somethingSelected = false;

        selected = false;
        sr.color = Color.white;

        if (Integrated())
        {
            //print("locked in");
            shapes[0].gameObject.layer = 9;
            shapes[1].gameObject.layer = 9;

            
            

            gridLayout.GetComponent<GridManager>().Click();
        }
        else
        {
            StartCoroutine(Drift());
            //sr.color = new Color(255, 255, 255, 0.6f);
            shapes[0].gameObject.layer = 10;
            shapes[1].gameObject.layer = 10;

        }
        sr.sortingOrder = 0;
        shapes[0].SortLayer(1);
        shapes[1].SortLayer(1);
    }

    IEnumerator Selected()
    {
        if (Integrated())
        {
            GetComponent<SpriteRenderer>().color = Color.green;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition );

        //follow mouse gridlocked
        if(vertical)
        {
            transform.position = gridLayout.WorldToCell(new Vector3(mousePos.x + 1f, mousePos.y , 0)   ) - new Vector3(0.5f, 0, 0);
        }
        else
        {
            transform.position = gridLayout.WorldToCell(new Vector3(mousePos.x + 0.5f, mousePos.y+0.5f , 0) ) - new Vector3(0, 0.5f, 0);
        }
        

        //rotate
        if (Input.GetMouseButtonDown(1))
        {
            vertical = !vertical;

            transform.Rotate(0, 0, 90);
            shapes[0].Rotate();
            shapes[1].Rotate();

            //print("rotate");
        }

        yield return new WaitForSeconds(0);

        if (selected)
        {
            StartCoroutine(Selected());
        }
    }

    IEnumerator Drift()
    {
        if (transform.position.y < -8)
        {
            Respawn();
        }

        transform.position += new Vector3(0, -driftSpeed * Time.deltaTime, 0);
        transform.Rotate(new Vector3(0, 0, 15) * Time.deltaTime * rotationDir);



        yield return new WaitForFixedUpdate();

        StartCoroutine(Drift());

    }
    


    bool Integrated()
    {
        int problems = 0;
        int empties = 0;
        //for each child symbol,
        for (int i = 0; i < shapes.Count; i++)
        {

            if (Physics2D.Raycast(shapes[i].transform.position, transform.up, 0.1f, shapeMask))
            {
                if(Physics2D.Raycast(shapes[i].transform.position, transform.up, 0.1f, shapeMask).collider.gameObject != gameObject)
                    return false;
                
                
            }

                //for each three adjacent spaces,
                for (int d = 0; d < shapes[i].directions.Count; d++)
            {
                shapeCheck = null;

                Vector3 direction = transform.TransformDirection(shapes[i].directions[d]);
                Debug.DrawLine(shapes[i].transform.position + direction, shapes[i].transform.position + direction + transform.up * 0.1f, Color.red, 2f);
                if (Physics2D.Raycast(shapes[i].transform.position + direction, transform.up, 0.1f, shapeMask))
                {
                    shapeCheck = Physics2D.Raycast(shapes[i].transform.position + direction, transform.up, 0.1f, shapeMask).collider.gameObject.GetComponent<Shape>();
                    //print("hit " + shapeCheck.transform.position);


                    if (shapeCheck != null)
                    {
                        //print("not null, name " + shapeCheck.name);

                        //check for match
                        if (shapeCheck.shapeName != shapes[i].shapeName)
                        {
                            //print("problem at " + i.ToString() + " & " + d);

                            //report mismatch
                            problems += 1;
                        }
                    }
                }
                else
                {
                    empties++;
                }
            }
        }

        //print("empties " + empties);

        if (problems == 0 && empties < shapes.Count * shapes[0].directions.Count)
        {
            //print("no problem");
            return true;
        }

        else
        {
            //print("problems. lots and lots of problems.");
            return false;
        }
    }
}