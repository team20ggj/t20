﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StrengthChecker : MonoBehaviour
{
    LayerMask shapeMask = 512;
    public Text text;
    public int strength = 0;

    private void Start()
    {
        text.transform.position = Camera.main.WorldToScreenPoint(transform.position);
    }


    public void ReportStrength()
    {
        strength = 0;
        RaycastHit2D[] shapeHits = Physics2D.RaycastAll(transform.position, transform.up, 20, shapeMask);

        for (int i = 0; i < shapeHits.Length; i++)
        {
            if(shapeHits[i].collider.GetComponent<Shape>().working == true)
            {
                strength++;
            }
        }

        text.text = strength.ToString();
    }


}
