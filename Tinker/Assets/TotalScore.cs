﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalScore : MonoBehaviour
{
    StrengthChecker[] allStrengths;
    public Text text;
    int minStrength;

    // Start is called before the first frame update
    void Start()
    {
        allStrengths = FindObjectsOfType<StrengthChecker>();
    }

    public void Refresh()
    {
        minStrength = 100;

        for (int s = 0; s < allStrengths.Length; s++)
        {
            if (allStrengths[s].strength < minStrength)
            {
                minStrength = allStrengths[s].strength;
            }

        }
        

        text.text = minStrength + "/3";
    }
}
